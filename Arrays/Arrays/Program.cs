﻿using System;

namespace Arrays
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Hello World!");
			//here is a new project to teach myself about arrays.

			int[] array1 = new int[5] { 1, 2, 3, 4, 5 };

			for (int i = 1; i <= 5; i++)
			{
				Console.WriteLine($"Array element {array1[i-1].ToString()} is {array1[i-1].ToString()}");
			}

			Console.WriteLine("");

			int x = 4;
			int y = 6;

			Console.WriteLine($"x = {x}; y = {y}");
			Console.WriteLine("x = {0}; y = {1}", x, y);
			Console.WriteLine("x = " + x.ToString() + "; y = " + y);
		}
	}
}
