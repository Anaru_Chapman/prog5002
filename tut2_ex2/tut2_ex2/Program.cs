﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tut2_ex2
{
    class Program
    {
        static void Main(string[] args)
        {
            var colour = "white";
            Console.WriteLine("Choose a colour, blue, red or something else ");
            colour = Console.ReadLine();
            switch (colour)
            {
                case "blue":
                    Console.WriteLine("You choose blue");
                    break;

                case "red":
                    Console.WriteLine("You choose red");
                    break;

                default:
                    Console.WriteLine("You choose something else");
                    break;
            

            }
            
        }
    }
}
