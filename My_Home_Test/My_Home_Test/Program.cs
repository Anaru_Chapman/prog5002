﻿using System;

namespace My_Home_Test
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var conversion_type =0; // declare the variables
			var number_for_conversion = 0.0;
			var converted_total = 0.0;

			Console.WriteLine("Enter 1 for km to miles or  2 for miles to km"); // ask for conversion type
			conversion_type = int.Parse(Console.ReadLine());					// convert to data type



			switch (conversion_type)						//switch statement for appropriate conversion.
			{
				case 1:
					Console.WriteLine("Enter the amount of km to convert");    
					number_for_conversion = int.Parse(Console.ReadLine());

					converted_total = number_for_conversion * 0.621;


					Console.WriteLine($"{number_for_conversion} km is equal to {converted_total} miles");
					break;
					
				case 2:
					Console.WriteLine("Enter the amount of miles to convert");
					number_for_conversion = int.Parse(Console.ReadLine());

					converted_total = number_for_conversion * 25;
					Console.WriteLine($"{number_for_conversion} miles is equal to {converted_total} km");
					break;
					
				default:
					Console.WriteLine("Default case");
					break;
			}
		}
	}
}
