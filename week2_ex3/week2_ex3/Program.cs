﻿using System;

namespace week2_ex3
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var counter = 5;
			var i = 0;

			for (i = 0; i < counter; i++)
			{
				int lineNumber = i + 1;
				Console.WriteLine($"This is line number {lineNumber}");
			}

			var answer = false;
			var score = 0;

			Console.WriteLine("The sky is blue.");
			answer = bool.Parse(Console.ReadLine());

			if (answer == true)
			{
				score++;
				Console.WriteLine($"Well done!your score is {score}");
			}

			else
			{
                score--;
                Console.WriteLine($"Sorry that is not correct! Your score is {score}");
			}

			Console.WriteLine("Variables in C# can start with a number");
			answer = bool.Parse(Console.ReadLine());

			if (answer == false)
			{
				score++;
				Console.WriteLine($"Well done!your score is {score}");
			}

			else
			{
                score--;
                Console.WriteLine($"Sorry that is not correct! Your score is {score}");
            }



		}
	}
}
